﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Microsoft.Phone.Controls.Primitives;

namespace Microsoft.Phone.Controls
{
    abstract class DataSource : ILoopingSelectorDataSource
    {
        private DateTimeWrapper _selectedItem;

        public object GetNext(object relativeTo)
        {
            DateTime? next;
            int delta = 1;
            do
            {
                next = GetRelativeTo(((DateTimeWrapper) relativeTo).DateTime, delta++);
            } while (next != null && BlackoutDates != null && BlackoutDates.Contains(next.Value));

            return next.HasValue ? new DateTimeWrapper(next.Value) : null;
        }

        public object GetPrevious(object relativeTo)
        {
            DateTime? next;
            int delta = -1;
            do
            {
                next = GetRelativeTo(((DateTimeWrapper) relativeTo).DateTime, delta--);
            } while (next != null && BlackoutDates != null && BlackoutDates.Contains(next.Value));

            return next.HasValue ? new DateTimeWrapper(next.Value) : null;
        }

        protected abstract DateTime? GetRelativeTo(DateTime relativeDate, int delta);

        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (value != _selectedItem)
                {
                    DateTimeWrapper valueWrapper = (DateTimeWrapper)value;
                    if ((null == valueWrapper) || (null == _selectedItem) || (valueWrapper.DateTime != _selectedItem.DateTime))
                    {
                        object previousSelectedItem = _selectedItem;
                        _selectedItem = valueWrapper;
                        var handler = SelectionChanged;
                        if (null != handler)
                        {
                            handler(this, new SelectionChangedEventArgs(new object[] { previousSelectedItem }, new object[] { _selectedItem }));
                        }
                    }
                }
            }
        }

        public DateTime? MaxDate { get; set; }

        public DateTime? MinDate { get; set; }

        public bool EndPointContinuousScroll { get; set; }

        public ICollection<DateTime> BlackoutDates { get; set; }

        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;
    }

    class YearDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            int minimumYear = 1601;
            int maximumYear = 3000;
            if ((minimumYear == relativeDate.Year && delta == -1) ||
                (maximumYear == relativeDate.Year && delta == 1))
            {
                return null;
            }

            int nextYear = relativeDate.Year + delta;

            if (MaxDate.HasValue && nextYear > MaxDate.Value.Year)
            {
                if (!EndPointContinuousScroll) return null;
                nextYear = MinDate.HasValue ? MinDate.Value.Year : minimumYear;
            }

            if (MinDate.HasValue && nextYear < MinDate.Value.Year)
            {
                if (!EndPointContinuousScroll) return null;
                nextYear = MaxDate.HasValue ? MaxDate.Value.Year : maximumYear;
            }

            int nextMonth = relativeDate.Month;
            if (MaxDate.HasValue &&
                nextMonth > MaxDate.Value.Month &&
                nextYear == MaxDate.Value.Year)
            {
                nextMonth = MaxDate.Value.Month;
            }

            if (MinDate.HasValue &&
                nextMonth < MinDate.Value.Month &&
                nextYear == MinDate.Value.Year)
            {
                nextMonth = MinDate.Value.Month;
            }

            int nextDay = Math.Min(relativeDate.Day, DateTime.DaysInMonth(nextYear, nextMonth));
            if (MaxDate.HasValue &&
                nextDay > MaxDate.Value.Day &&
                nextYear == MaxDate.Value.Year &&
                nextMonth == MaxDate.Value.Month)
            {
                nextDay = MaxDate.Value.Day;
            }

            if (MinDate.HasValue &&
                nextDay < MinDate.Value.Day &&
                nextYear == MinDate.Value.Year &&
                nextMonth == MinDate.Value.Month)
            {
                nextDay = MinDate.Value.Day;
            }

            return new DateTime(nextYear, nextMonth, nextDay, relativeDate.Hour, relativeDate.Minute, relativeDate.Second);
        }
    }

    class MonthDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            var sameYear = (MinDate.HasValue && MaxDate.HasValue && MinDate.Value.Year == MaxDate.Value.Year);

            int monthsInYear = 12;
            int nextMonth = ((monthsInYear + relativeDate.Month - 1 + delta) % monthsInYear) + 1;
            if (MaxDate.HasValue &&
                nextMonth > MaxDate.Value.Month &&
                relativeDate.Year == MaxDate.Value.Year)
            {
                if (!EndPointContinuousScroll) return null;
                nextMonth = delta > 0 ? (sameYear ? MinDate.Value.Month : 1): MaxDate.Value.Month;
            }

            if(MinDate.HasValue &&
                nextMonth < MinDate.Value.Month &&
                relativeDate.Year == MinDate.Value.Year)
            {
                if (!EndPointContinuousScroll) return null;
                nextMonth = delta < 0 ? (sameYear ? MaxDate.Value.Month : monthsInYear) : MinDate.Value.Month;
            }

            int nextDay = Math.Min(relativeDate.Day, DateTime.DaysInMonth(relativeDate.Year, nextMonth));
            if (MaxDate.HasValue &&
                nextDay > MaxDate.Value.Day &&
                relativeDate.Year == MaxDate.Value.Year &&
                nextMonth == MaxDate.Value.Month)
            {
                nextDay = MaxDate.Value.Day;
            }

            if (MinDate.HasValue &&
                nextDay < MinDate.Value.Day &&
                relativeDate.Year == MinDate.Value.Year &&
                nextMonth == MinDate.Value.Month)
            {
                nextDay = MinDate.Value.Day;
            }

            return new DateTime(relativeDate.Year, nextMonth, nextDay, relativeDate.Hour, relativeDate.Minute, relativeDate.Second);
        }
    }

    class DayDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            var sameMonth = (MinDate.HasValue && MaxDate.HasValue && MinDate.Value.Year == MaxDate.Value.Year &&
                             MinDate.Value.Month == MaxDate.Value.Month);
            int daysInMonth = DateTime.DaysInMonth(relativeDate.Year, relativeDate.Month);
            int nextDay = ((daysInMonth + relativeDate.Day - 1 + delta) % daysInMonth) + 1;
            if (MaxDate.HasValue &&
                nextDay > MaxDate.Value.Day &&
                relativeDate.Year == MaxDate.Value.Year &&
                relativeDate.Month == MaxDate.Value.Month)
            {
                if (!EndPointContinuousScroll) return null;
                nextDay = delta > 0 ? (sameMonth ? MinDate.Value.Day : 1) : MaxDate.Value.Day;
            }

            if(MinDate.HasValue &&
                nextDay < MinDate.Value.Day &&
                relativeDate.Year == MinDate.Value.Year &&
                relativeDate.Month == MinDate.Value.Month)
            {
                if (!EndPointContinuousScroll) return null;
                nextDay = delta < 0 ? (sameMonth ? MaxDate.Value.Day : daysInMonth) : MinDate.Value.Day;
            }

            return new DateTime(relativeDate.Year, relativeDate.Month, nextDay, relativeDate.Hour, relativeDate.Minute, relativeDate.Second);
        }
    }

    class TwelveHourDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            int hoursInHalfDay = 12;
            int nextHour = (hoursInHalfDay + relativeDate.Hour + delta) % hoursInHalfDay;
            nextHour += hoursInHalfDay <= relativeDate.Hour ? hoursInHalfDay : 0;
            return new DateTime(relativeDate.Year, relativeDate.Month, relativeDate.Day, nextHour, relativeDate.Minute, 0);
        }
    }

    class MinuteDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            int minutesInHour = 60;
            int nextMinute = (minutesInHour + relativeDate.Minute + delta) % minutesInHour;
            return new DateTime(relativeDate.Year, relativeDate.Month, relativeDate.Day, relativeDate.Hour, nextMinute, 0);
        }
    }

    class AmPmDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            int hoursInDay = 24;
            int nextHour = relativeDate.Hour + (delta * (hoursInDay / 2));
            if ((nextHour < 0) || (hoursInDay <= nextHour))
            {
                return null;
            }
            return new DateTime(relativeDate.Year, relativeDate.Month, relativeDate.Day, nextHour, relativeDate.Minute, 0);
        }
    }

    class TwentyFourHourDataSource : DataSource
    {
        protected override DateTime? GetRelativeTo(DateTime relativeDate, int delta)
        {
            int hoursInDay = 24;
            int nextHour = (hoursInDay + relativeDate.Hour + delta) % hoursInDay;
            return new DateTime(relativeDate.Year, relativeDate.Month, relativeDate.Day, nextHour, relativeDate.Minute, 0);
        }
    }
}
