﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;
using Microsoft.Phone.Controls.DateTimeConverters;
using Microsoft.Phone.Controls.Primitives;

namespace Microsoft.Phone.Controls
{
    /// <summary>
    /// Represents a base class for controls that allow the user to choose a date/time.
    /// </summary>
    [TemplatePart(Name = ButtonPartName, Type = typeof(ButtonBase))]
    public class DateTimePickerBase : Control
    {
        private const string ButtonPartName = "DateTimeButton";

        private ButtonBase _dateButtonPart;
        private PhoneApplicationFrame _frame;
        private object _frameContentWhenOpened;
        private NavigationInTransition _savedNavigationInTransition;
        private NavigationOutTransition _savedNavigationOutTransition;
        private IDateTimePickerPage _dateTimePickerPage;

        /// <summary>
        /// Event that is invoked when the Value property changes.
        /// </summary>
        public event EventHandler<DateTimeValueChangedEventArgs> ValueChanged;

        /// <summary>
        /// Gets or sets the DateTime value.
        /// </summary>
        [TypeConverter(typeof(TimeTypeConverter))]
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods", Justification = "Matching the use of Value as a Picker naming convention.")]
        public DateTime? Value
        {
            get { return (DateTime?)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        /// <summary>
        /// Identifies the Value DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            "Value", typeof(DateTime?), typeof(DateTimePickerBase), new PropertyMetadata(null, OnValueChanged));

        private static void OnValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePickerBase)o).OnValueChanged((DateTime?)e.OldValue, (DateTime?)e.NewValue);
        }

        private void OnValueChanged(DateTime? oldValue, DateTime? newValue)
        {
            UpdateValueString();
            OnValueChanged(new DateTimeValueChangedEventArgs(oldValue, newValue));
        }

        /// <summary>
        /// Called when the value changes.
        /// </summary>
        /// <param name="e">The event data.</param>
        protected virtual void OnValueChanged(DateTimeValueChangedEventArgs e)
        {
            var handler = ValueChanged;
            if (null != handler)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Gets the string representation of the selected value.
        /// </summary>
        public string ValueString
        {
            get { return (string)GetValue(ValueStringProperty); }
            private set { SetValue(ValueStringProperty, value); }
        }

        /// <summary>
        /// Identifies the ValueString DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty ValueStringProperty = DependencyProperty.Register(
            "ValueString", typeof(string), typeof(DateTimePickerBase), null);

        /// <summary>
        /// Gets or sets the format string to use when converting the Value property to a string.
        /// </summary>
        public string ValueStringFormat
        {
            get { return (string)GetValue(ValueStringFormatProperty); }
            set { SetValue(ValueStringFormatProperty, value); }
        }

        /// <summary>
        /// Identifies the ValueStringFormat DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty ValueStringFormatProperty = DependencyProperty.Register(
            "ValueStringFormat", typeof(string), typeof(DateTimePickerBase), new PropertyMetadata(null, OnValueStringFormatChanged));

        private static void OnValueStringFormatChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePickerBase)o).OnValueStringFormatChanged(/*(string)e.OldValue, (string)e.NewValue*/);
        }

        private void OnValueStringFormatChanged(/*string oldValue, string newValue*/)
        {
            UpdateValueString();
        }

        /// <summary>
        /// Gets or sets the header of the control.
        /// </summary>
        public object Header
        {
            get { return (object)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        /// <summary>
        /// Identifies the Header DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register(
            "Header", typeof(object), typeof(DateTimePickerBase), null);

        /// <summary>
        /// Gets or sets the template used to display the control's header.
        /// </summary>
        public DataTemplate HeaderTemplate
        {
            get { return (DataTemplate)GetValue(HeaderTemplateProperty); }
            set { SetValue(HeaderTemplateProperty, value); }
        }

        /// <summary>
        /// Identifies the HeaderTemplate DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty HeaderTemplateProperty = DependencyProperty.Register(
            "HeaderTemplate", typeof(DataTemplate), typeof(DateTimePickerBase), null);

        /// <summary>
        /// Gets or sets the Uri to use for loading the IDateTimePickerPage instance when the control is clicked.
        /// </summary>
        public Uri PickerPageUri
        {
            get { return (Uri)GetValue(PickerPageUriProperty); }
            set { SetValue(PickerPageUriProperty, value); }
        }

        /// <summary>
        /// Identifies the PickerPageUri DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty PickerPageUriProperty = DependencyProperty.Register(
            "PickerPageUri", typeof(Uri), typeof(DateTimePickerBase), null);

        /// <summary>
        /// Gets or sets if the selector should scroll continuously repeat values when it reaches DisplayDateStart or DisplayDateEnd.
        /// </summary>
        public bool DisplayDateEndPointContinuousScroll
        {
            get { return (bool) GetValue(DisplayDateEndPointContinuousScrollProperty); }
            set { SetValue(DisplayDateEndPointContinuousScrollProperty, value); }
        }

        /// <summary>
        /// Identifies the DisplayDateEndPointContinuousScroll property.
        /// </summary>
        public static readonly DependencyProperty DisplayDateEndPointContinuousScrollProperty = DependencyProperty.Register(
            "DisplayDateEndPointContinuousScroll", typeof (bool), typeof (DateTimePickerBase), null);

        /// <summary>
        /// Gets or sets the last date to be displayed.
        /// </summary>
        [TypeConverter(typeof(DateTimeTypeConverter))]
        public DateTime? DisplayDateEnd
        {
            get { return (DateTime?)GetValue(DisplayDateEndProperty); }
            set { SetValue(DisplayDateEndProperty, value); }
        }

        /// <summary>
        /// Identifies the DisplayDateEnd DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty DisplayDateEndProperty = DependencyProperty.Register(
            "DisplayDateEnd", typeof(DateTime?), typeof(DateTimePickerBase), new PropertyMetadata(null, OnDisplayDateEndChanged));

        private static void OnDisplayDateEndChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((DateTime?)e.NewValue < ((DateTimePickerBase)d).DisplayDateStart)
                throw new ArgumentException("DisplayDateEnd can not precede DisplayDateStart", "DisplayDateEnd");

            if ((DateTime?) e.NewValue < ((DateTimePickerBase) d).Value)
                ((DateTimePickerBase)d).Value = (DateTime?) e.NewValue;
        }

        /// <summary>
        /// Gets or sets the first date to be displayed.
        /// </summary>
        [TypeConverter(typeof(DateTimeTypeConverter))]
        public DateTime? DisplayDateStart
        {
            get { return (DateTime?) GetValue(DisplayDateStartProperty); }
            set { SetValue(DisplayDateStartProperty, value);}
        }

        /// <summary>
        /// Identifies the DisplayDateStart DependencyProperty.
        /// </summary>
        public static readonly DependencyProperty DisplayDateStartProperty = DependencyProperty.Register(
            "DisplayDateStart", typeof (DateTime?), typeof (DateTimePickerBase), new PropertyMetadata(null, OnDisplayDateStartChanged));

        private static void OnDisplayDateStartChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((DateTime?)e.NewValue > ((DateTimePickerBase)d).DisplayDateEnd)
                throw new ArgumentException("DisplayDateStart can not succeed DisplayDateEnd", "DisplayDateStart");

            if ((DateTime?)e.NewValue > ((DateTimePickerBase)d).Value)
                ((DateTimePickerBase)d).Value = (DateTime?)e.NewValue;
        }

        /// <summary>
        /// Gets or sets a collection of dates that are marked as not selectable.
        /// </summary>
        [TypeConverter(typeof(BlackoutDatesConverter))]
        public ICollection<DateTime> BlackoutDates
        {
            get { return (ICollection<DateTime>) GetValue(BlackoutDatesProperty); }
            set { SetValue(BlackoutDatesProperty, value); }
        }

        /// <summary>
        /// Identifies the BlackoutDates DependencyProperty
        /// </summary>
        public static readonly DependencyProperty BlackoutDatesProperty = DependencyProperty.Register(
            "BlackoutDates", typeof (ICollection<DateTime>), typeof (DateTimePickerBase), new PropertyMetadata(default(ICollection<DateTime>), OnBlackoutDatesChanged));

        private static void OnBlackoutDatesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(e.NewValue != null)
            {
                if (((DateTimePickerBase)d).Value.HasValue)
                {
                    var nonBlackoutValue = ((DateTimePickerBase)d).GetNonBlackoutDate(((DateTimePickerBase)d).Value.Value, 1, ((DateTimePickerBase)d).DisplayDateEnd.GetValueOrDefault(new DateTime(3000,1,1))) ??
                                           ((DateTimePickerBase)d).GetNonBlackoutDate(((DateTimePickerBase)d).Value.Value, -1, ((DateTimePickerBase)d).DisplayDateStart.GetValueOrDefault(new DateTime(1600, 1, 1)));

                    ((DateTimePickerBase) d).Value = nonBlackoutValue;
                }
            }
        }

        /// <summary>
        /// Gets the fallback value for the ValueStringFormat property.
        /// </summary>
        protected virtual string ValueStringFormatFallback { get { return "{0}"; } }

        /// <summary>
        /// Initializes a new instance of the DateTimePickerBase control.
        /// </summary>
        public DateTimePickerBase()
        {
        }

        /// <summary>
        /// Called when the control's Template is expanded.
        /// </summary>
        public override void OnApplyTemplate()
        {
            // Unhook from old template
            if (null != _dateButtonPart)
            {
                _dateButtonPart.Click -= OnDateButtonClick;
            }

            base.OnApplyTemplate();

            // Hook up to new template
            _dateButtonPart = GetTemplateChild(ButtonPartName) as ButtonBase;
            if (null != _dateButtonPart)
            {
                _dateButtonPart.Click += OnDateButtonClick;
            }
        }

        private void OnDateButtonClick(object sender, RoutedEventArgs e)
        {
            OpenPickerPage();
        }

        private void UpdateValueString()
        {
            ValueString = string.Format(CultureInfo.CurrentCulture, ValueStringFormat ?? ValueStringFormatFallback, Value);
        }

        private void OpenPickerPage()
        {
            if (null == PickerPageUri)
            {
                throw new ArgumentException("PickerPageUri property must not be null.");
            }

            if (null == _frame)
            {
                // Hook up to necessary events and navigate
                _frame = Application.Current.RootVisual as PhoneApplicationFrame;
                if (null != _frame)
                {
                    _frameContentWhenOpened = _frame.Content;

                    // Save and clear host page transitions for the upcoming "popup" navigation
                    UIElement frameContentWhenOpenedAsUIElement = _frameContentWhenOpened as UIElement;
                    if (null != frameContentWhenOpenedAsUIElement)
                    {
                        _savedNavigationInTransition = TransitionService.GetNavigationInTransition(frameContentWhenOpenedAsUIElement);
                        TransitionService.SetNavigationInTransition(frameContentWhenOpenedAsUIElement, null);
                        _savedNavigationOutTransition = TransitionService.GetNavigationOutTransition(frameContentWhenOpenedAsUIElement);
                        TransitionService.SetNavigationOutTransition(frameContentWhenOpenedAsUIElement, null);
                    }

                    _frame.Navigated += OnFrameNavigated;
                    _frame.NavigationStopped += OnFrameNavigationStoppedOrFailed;
                    _frame.NavigationFailed += OnFrameNavigationStoppedOrFailed;

                    _frame.Navigate(PickerPageUri);
                }
            }

        }

        private void ClosePickerPage()
        {
            // Unhook from events
            if (null != _frame)
            {
                _frame.Navigated -= OnFrameNavigated;
                _frame.NavigationStopped -= OnFrameNavigationStoppedOrFailed;
                _frame.NavigationFailed -= OnFrameNavigationStoppedOrFailed;

                // Restore host page transitions for the completed "popup" navigation
                UIElement frameContentWhenOpenedAsUIElement = _frameContentWhenOpened as UIElement;
                if (null != frameContentWhenOpenedAsUIElement)
                {
                    TransitionService.SetNavigationInTransition(frameContentWhenOpenedAsUIElement, _savedNavigationInTransition);
                    _savedNavigationInTransition = null;
                    TransitionService.SetNavigationOutTransition(frameContentWhenOpenedAsUIElement, _savedNavigationOutTransition);
                    _savedNavigationOutTransition = null;
                }

                _frame = null;
                _frameContentWhenOpened = null;
            }
            // Commit the value if available
            if (null != _dateTimePickerPage)
            {
                if(_dateTimePickerPage.Value.HasValue)
                {
                    Value = _dateTimePickerPage.Value.Value;
                }
                _dateTimePickerPage = null;
            }
        }

        private void OnFrameNavigated(object sender, NavigationEventArgs e)
        {
            if (e.Content == _frameContentWhenOpened)
            {
                // Navigation to original page; close the picker page
                ClosePickerPage();
            }
            else if (null == _dateTimePickerPage)
            {
                // Navigation to a new page; capture it and push the value in
                _dateTimePickerPage = e.Content as IDateTimePickerPage;
                if (null != _dateTimePickerPage)
                {
                    var inputValue = Value.GetValueOrDefault(DateTime.Now);
                    if (inputValue > DisplayDateEnd)
                        inputValue = DisplayDateEnd.Value;
                    if (inputValue < DisplayDateStart)
                        inputValue = DisplayDateStart.Value;

                    var nonBlackoutInput = GetNonBlackoutDate(inputValue, 1, DisplayDateEnd.GetValueOrDefault(new DateTime(3000, 1, 1))) ??
                                           GetNonBlackoutDate(inputValue, -1, DisplayDateStart.GetValueOrDefault(new DateTime(1600, 1, 1)));

                    if (!nonBlackoutInput.HasValue)
                    {
                        throw new ArgumentException("No valid start date available when BlackoutDates have been applied");
                    }

                    _dateTimePickerPage.Value = nonBlackoutInput.Value;
                    _dateTimePickerPage.MaxDate = DisplayDateEnd;
                    _dateTimePickerPage.MinDate = DisplayDateStart;
                    _dateTimePickerPage.EndPointContinuousScroll = DisplayDateEndPointContinuousScroll;
                    _dateTimePickerPage.BlackoutDates = BlackoutDates;
                }
            }
        }

        private DateTime? GetNonBlackoutDate(DateTime startDate, int dayInterval, DateTime endDate)
        {
            if (BlackoutDates == null)
                return startDate;

            DateTime? currentDateInSearch = startDate;
            do
            {
                if(dayInterval > 0)
                {
                    if (currentDateInSearch.Value.Date > endDate.Date)
                        currentDateInSearch = null;
                }
                else
                {
                    if (currentDateInSearch.Value.Date < endDate.Date)
                        currentDateInSearch = null;
                }

                if (!BlackoutDates.Any(blackoutDate => blackoutDate.Date == currentDateInSearch.Value.Date))
                    return currentDateInSearch;

                currentDateInSearch = currentDateInSearch.Value.AddDays(dayInterval);
            } while (currentDateInSearch != null);

            return currentDateInSearch;
        }

        private void OnFrameNavigationStoppedOrFailed(object sender, EventArgs e)
        {
            // Abort
            ClosePickerPage();
        }
    }
}
