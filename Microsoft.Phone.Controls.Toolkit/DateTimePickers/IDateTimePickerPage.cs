﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Collections.Generic;

namespace Microsoft.Phone.Controls.Primitives
{
    /// <summary>
    /// Represents an interface for DatePicker/TimePicker to use for communicating with a picker page.
    /// </summary>
    public interface IDateTimePickerPage
    {
        /// <summary>
        /// Gets or sets the DateTime to show in the picker page and to set when the user makes a selection.
        /// </summary>
        DateTime? Value { get; set; }

        /// <summary>
        /// Gets or sets the DateTime to limit the picker page maximum user selection
        /// </summary>
        DateTime? MaxDate { get; set; }

        /// <summary>
        /// Gets or sets the DateTime to limit the picker page minimum user selection
        /// </summary>
        DateTime? MinDate { get; set; }

        /// <summary>
        /// Gets or sets if the user selection should continuously scroll when it reaches MinDate or MaxDate
        /// </summary>
        bool EndPointContinuousScroll { get; set; }

        /// <summary>
        /// Gets or sets a collection of dates that are marked as not selectable.
        /// </summary>
        ICollection<DateTime> BlackoutDates { get; set; }
    }
}
