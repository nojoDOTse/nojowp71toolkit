﻿// (c) Copyright Microsoft Corporation.
// This source is subject to the Microsoft Public License (Ms-PL).
// Please see http://go.microsoft.com/fwlink/?LinkID=131993 for details.
// All other rights reserved.

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Windows.Markup;

[assembly: AssemblyTitle("nojo.se.nojoWP71Toolkit")]
[assembly: AssemblyDescription("Windows Phone 7.1 Toolkit")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("© Microsoft Corporation.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("55ef827a-1624-4b11-84c3-3bd8d9bcca7b")]

[assembly: AssemblyVersion("1.0.1")]
[assembly: AssemblyFileVersion("1.0.1")]

[assembly: CLSCompliant(false)] // IApplicationBar is not CLS-compliant, but its use matches the type of the platform's PhoneApplicationPage.ApplicationBar property
[assembly: NeutralResourcesLanguage("en-US")]

[assembly: XmlnsPrefix("clr-namespace:Microsoft.Phone.Controls;assembly=nojo.se.nojoWP71Toolkit", "toolkit")]
[assembly: XmlnsDefinition("clr-namespace:Microsoft.Phone.Controls;assembly=nojo.se.nojoWP71Toolkit", "Microsoft.Phone.Controls")]
[assembly: XmlnsPrefix("clr-namespace:Microsoft.Phone.Controls.Primitives;assembly=nojo.se.nojoWP71Toolkit", "toolkitPrimitives")]
[assembly: XmlnsDefinition("clr-namespace:Microsoft.Phone.Controls.Primitives;assembly=nojo.se.nojoWP71Toolkit", "Microsoft.Phone.Controls.Primitives")]
