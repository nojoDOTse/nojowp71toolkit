# nojoWP71Toolkit

nojoWP71Toolkit is a fork of the Windows Phone 7.1 part the [Silverlight Toolkit][1] by [Microsoft][2]. It is currently based on [Change Set 71382][4] which is the [latest stable release][5].

## Added functionality

###DatePicker

New property `DisplayDateStart` that defines the minimum possible date to select.

    :::xml
    <DatePicker DisplayDateStart="1999/11/15" />

New property `DisplayDateEnd` that defines the maximum possible date to select.

    :::xml
    <DatePicker DisplayDateEnd="2013/03/06" />

## Downloads
Available as a [NuGet package][6] on the [nojoDOTse MyGet feed][7]. Add `http://www.myget.org/F/nojodotse/` as a package source for NuGet for easier downloads.

The `nupkg` files are also available in the [Downloads section on BitBucket][8]

## Why a fork?

The team behind Silverlight Toolkit have [stated][1] that they can not accept any source code sent to them. Since there will be no integration of community-created patches this fork was created for just that.

## License

nojoWP71Toolkit is licensed under the [Microsoft Public License][3]. A copy of the license is available in the file `LICENSE.txt` found in the root of the repository.

[1]: http://silverlight.codeplex.com/
[2]: http://www.microsoft.com/
[3]: http://www.opensource.org/licenses/MS-PL
[4]: http://silverlight.codeplex.com/SourceControl/changeset/changes/71382
[5]: http://silverlight.codeplex.com/releases/view/75888
[6]: http://www.myget.org/feed/nojodotse/package/nojoWP71Toolkit
[7]: http://www.myget.org/F/nojodotse/
[8]: https://bitbucket.org/nojoDOTse/nojowp71toolkit/downloads